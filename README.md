# Overview

Sublime Text 3 plugin to show GitLab vulnerabilities for a project in line
with the source code.

# Installation

Create a file called `.gitlab_config` in the same directory as this plugin.
That file should contain two lines:
   1. The GitLab API token you want to use
   1. The GitLab project path you want to refer to. Note it must be URL escaped.
   An example would be `stkerr%2Fcustom-scanner`

# Screenshot examples

![screenshot](screenshot.png)
