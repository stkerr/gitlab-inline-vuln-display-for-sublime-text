import sublime
import sublime_plugin
import html
import requests
import json
import os

# Since class name is GitlabvulnsCommand, command is 'gitlabvulns'.
# 'GitLabVulns Command' wont parse correctly
class GitlabvulnsCommand(sublime_plugin.TextCommand):

	def on_phantom_navigate(self, url):
		print(url)
		self.view.erase_phantoms(url)

	def run(self, edit):
		with open(os.getcwd() + os.sep + '.gitlab_config','r') as f:
			lines = f.readlines()
			self._TOKEN = lines[0]
			self._PROJECT_NAME = lines[1]

		stylesheet = '''
			<style>
				div.error-arrow {
					border-top: 0.4rem solid transparent;
					border-left: 0.5rem solid color(var(--redish) blend(var(--background) 30%));
					width: 0;
					height: 0;
				}
				div.error {
					padding: 0.4rem 0 0.4rem 0.7rem;
					margin: 0 0 0.2rem;
					border-radius: 0 0.2rem 0.2rem 0.2rem;
				}
				div.error span.message {
					padding-right: 0.7rem;
				}
				div.error a {
					text-decoration: inherit;
					padding: 0.35rem 0.7rem 0.45rem 0.8rem;
					position: relative;
					bottom: 0.05rem;
					border-radius: 0 0.2rem 0.2rem 0;
					font-weight: bold;
				}
				html.dark div.error a {
					background-color: #00000018;
				}
				html.light div.error a {
					background-color: #ffffff18;
				}
			</style>
		'''
		print('here')

		vulns = requests.get(
			'https://gitlab.com/api/v4/projects/%s/vulnerability_findings' % self._PROJECT_NAME,
			headers = {
				'PRIVATE-TOKEN': self._TOKEN
			}
		).text

		vulns = json.loads(vulns)

		phantom_counter = 0

		for vuln in vulns:
			loc = vuln['location']['file']

			print("Vuln in file %s:%s" % (loc, vuln['location']['start_line']))

			print(loc)
			print(self.view.file_name())
			try:
				if os.path.basename(loc) != os.path.basename(self.view.file_name()):
					continue
			except:
				continue

			# Useful APIs
			# self.view.add_phantoms(<key string>, region, content, layout)
			# self.view.erase_phantoms(<key string>)
			# self.view.query_phantom(<phantom ID #>)
			# self.view.query_phantoms([<phantom ID #>, <phantom ID #>])

			#self.view.erase_phantoms('gitlabvuln')

			text = vuln['name']
			result = self.view.add_phantom(
				key="gitlabvuln%d" % phantom_counter,
				#region=self.view.sel()[0],
				region=self.view.line(self.view.text_point(vuln['location']['start_line'],0)),
				content=
				'<body id=inline-error>' + stylesheet +
							'<div class="error-arrow"></div><div class="error">' +
							'<span class="message">' + html.escape(text, quote=False) + '</span>' +
							'<a href=gitlabvuln%d>' %phantom_counter + chr(0x00D7) + '</a></div>' +
							'</body>',
				layout=sublime.LAYOUT_INLINE,
				on_navigate=self.on_phantom_navigate
			)

			phantom_counter = phantom_counter + 1

			'''
			self.view.add_regions(
				"gitlabvulns",
				[sublime.Region(a,b)],
				"mark",
				"dot"
			)
			'''


